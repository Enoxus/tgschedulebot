package ru.kpfu.tgBot;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ScheduleBot extends TelegramLongPollingBot {
    private static final String GROUP1 = "11-801";
    private static final String GROUP2 = "11-802";
    private static final String GROUP3 = "11-803";
    private static final String GROUP4 = "11-804";
    private static final String GROUP5 = "11-805";
    private static final String GROUP6 = "11-806";
    private static final String GROUP7 = "11-807";
    private static final String GROUP8 = "11-808";
    private static final String GROUP9 = "11-809";
    private static final String GROUP10 = "11-810";
    private static final String GROUP11 = "11-811";
    private static final String TODAY = "Сегодня";
    private static final String TOMORROW = "Завтра";
    private static final String BACK = "Назад";

    private static final List<String> groups = new ArrayList<String>();
    private static final List<String> dates = new ArrayList<String>();

    /* Так как боту нужно получить 2 сообщения для вывода информации,
        требуется где-то хранить "сессию" с одним пользователем, для этого
        используется HashMap, где ключом является chatID (уникальный для каждого пользователя),
        а значением уже введенные данные (вся логика в onUpdateReceived()
     */
    private static HashMap<Long, String> queue = new HashMap<Long, String>();

    static {
        groups.add(GROUP1);
        groups.add(GROUP2);
        groups.add(GROUP3);
        groups.add(GROUP4);
        groups.add(GROUP5);
        groups.add(GROUP6);
        groups.add(GROUP7);
        groups.add(GROUP8);
        groups.add(GROUP9);
        groups.add(GROUP10);
        groups.add(GROUP11);

        dates.add(TODAY);
        dates.add(TOMORROW);
    }

    public String getBotToken() {
        return GAPI.TELEGRAM_TOKEN;
    }


    public void onUpdateReceived(Update update) {
        Long chatID = update.getMessage().getChatId();
        String name = update.getMessage().getChat().getUserName(); //Сохраняет юзернейм пользователя для логов
        if (update.hasMessage() && update.getMessage().hasText()) {
            String msg = update.getMessage().getText();
            if (msg.equals("/start")) {
                SendMessage message = new SendMessage()
                        .setChatId(update.getMessage().getChatId())
                        .setText("Добро пожаловать! Выберите свою группу")
                        .setReplyMarkup(generateGroupKeyboard());
                try {
                    execute(message);
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else {
                if (groups.contains(msg)) {
                    queue.put(chatID, msg);
                    SendMessage message = new SendMessage()
                            .setChatId(update.getMessage().getChatId())
                            .setText("Введите дату")
                            .setReplyMarkup(generateDateKeyboard());
                    try {
                        execute(message);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (dates.contains(msg)) {
                    queue.put(chatID, queue.get(chatID) + " " + msg);
                } else if (msg.equals("Назад")) {
                    queue.remove(chatID);
                    SendMessage message = new SendMessage()
                            .setChatId(update.getMessage().getChatId())
                            .setText("Выберите группу")
                            .setReplyMarkup(generateGroupKeyboard());
                    try {
                        execute(message);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else {
                    queue.remove(chatID);
                    SendMessage message = new SendMessage()
                            .setChatId(update.getMessage().getChatId())
                            .setText("Введена неизвестная команда")
                            .setReplyMarkup(generateGroupKeyboard());
                    try {
                        execute(message);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

            }
            if (queue.containsKey(chatID)) {  // Проверяется, получена ли вся необходимая информация для ответа
                if (queue.get(chatID).contains(" ")) {
                    if (queue.get(chatID).split(" ").length == 2) {
                        System.out.println("Got the chat with " + name + " at " + new Date());
                        String group = queue.get(chatID).split(" ")[0];
                        String day = queue.get(chatID).split(" ")[1];
                        String response = " ";
                        try {
                            response = Parser.processRequest(new ScheduleRequest(group, day));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        SendMessage message = new SendMessage()
                                .setChatId(update.getMessage().getChatId())
                                .setText(response)
                                .setReplyMarkup(generateGroupKeyboard());
                        try {
                            execute(message);
                        } catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                        queue.remove(chatID);
                    }
                }
            }
        }
    }

    // Генератор клавиатуры выбора группы
    public ReplyKeyboardMarkup generateGroupKeyboard() {
        ReplyKeyboardMarkup keyBoardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        KeyboardRow row1 = new KeyboardRow();
        KeyboardRow row2 = new KeyboardRow();
        KeyboardRow row3 = new KeyboardRow();
        KeyboardRow row4 = new KeyboardRow();

        //1st row content

        row1.add(GROUP1);
        row1.add(GROUP2);
        row1.add(GROUP3);

        //2nd row content

        row2.add(GROUP4);
        row2.add(GROUP5);
        row2.add(GROUP6);

        //3rd row content

        row3.add(GROUP7);
        row3.add(GROUP8);
        row3.add(GROUP9);

        //4th row content

        row4.add(GROUP10);
        row4.add(GROUP11);


        keyboard.add(row1);
        keyboard.add(row2);
        keyboard.add(row3);
        keyboard.add(row4);
        keyBoardMarkup.setSelective(true);
        keyBoardMarkup.setResizeKeyboard(true);
        keyBoardMarkup.setOneTimeKeyboard(true);
        keyBoardMarkup.setKeyboard(keyboard);
        keyBoardMarkup.setResizeKeyboard(true);

        return keyBoardMarkup;
    }

    // Генератор клавиатуры выбора даты
    public ReplyKeyboardMarkup generateDateKeyboard() {
        ReplyKeyboardMarkup keyBoardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
        KeyboardRow row1 = new KeyboardRow();

        row1.add(TODAY);
        row1.add(TOMORROW);
        row1.add(BACK);

        keyboard.add(row1);

        keyBoardMarkup.setSelective(true);
        keyBoardMarkup.setResizeKeyboard(true);
        keyBoardMarkup.setOneTimeKeyboard(true);
        keyBoardMarkup.setKeyboard(keyboard);
        keyBoardMarkup.setResizeKeyboard(true);

        return keyBoardMarkup;
    }


    public String getBotUsername() {
        return GAPI.TELEGRAM_USER;
    }
}
