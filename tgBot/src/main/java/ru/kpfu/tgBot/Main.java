package ru.kpfu.tgBot;

import org.json.simple.parser.ParseException;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.IOException;
import java.util.Date;

public class Main {

    public static void main(String[] args) throws ParseException, IOException {
        Parser.update(); //initial update
        ApiContextInitializer.init();
        TelegramBotsApi botapi = new TelegramBotsApi();
        try {
            botapi.registerBot(new ScheduleBot());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
        Updater updater = new Updater();
        System.out.println("Data updated, bot launched, updater initialized " + new Date());
        updater.run(); // launching daily updater
    }

    public static class Updater extends Thread {
        @Override
        public void run() {
            super.run();
            while (!isInterrupted()) {
                try {
                    Thread.sleep(60 * 60 * 1000); // 1 hour sleep to check
                    Date currentDate = new Date();
                    if (currentDate.getHours() == 0) {
                        Parser.update();
                        System.out.println("Data updated at " + currentDate);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

