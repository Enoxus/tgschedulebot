package ru.kpfu.tgBot;

public class ScheduleRequest {
    private String group;
    private String date;

    public ScheduleRequest(String group, String date) {
        this.group = group;
        this.date = date;
    }

    public String getGroup() {
        return group;
    }

    public String getDate() {
        return date;
    }
}
