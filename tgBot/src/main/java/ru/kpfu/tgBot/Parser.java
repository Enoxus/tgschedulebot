package ru.kpfu.tgBot;

import net.dongliu.requests.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


public class Parser {
    private static HashMap<String, String> daysOfWeek = new HashMap<String, String>();
    private static ArrayList<String> dOW = new ArrayList<String>();

    // Переменные ниже хранят расписание, полученное методом update()
    private static JSONArray todayArr;
    private static JSONArray tomorrowArr;

    static {
        daysOfWeek.put("Mon", "!B4:M10");
        daysOfWeek.put("Tue", "!B11:M17");
        daysOfWeek.put("Wed", "!B18:M24");
        daysOfWeek.put("Thu", "!B25:M31");
        daysOfWeek.put("Fri", "!B32:M38");
        daysOfWeek.put("Sat", "!B39:M44");
        daysOfWeek.put("Sun", "!B39:M44");

        dOW.add("Mon");
        dOW.add("Tue");
        dOW.add("Wed");
        dOW.add("Thu");
        dOW.add("Fri");
        dOW.add("Sat");
        dOW.add("Sun");
    }


    public static synchronized JSONArray getTodayArr() {
        return todayArr;
    }

    public static synchronized JSONArray getTomorrowArr() {
        return tomorrowArr;
    }

    public synchronized static void setTodayArr(JSONArray todayArr) {
        Parser.todayArr = todayArr;
    }

    public synchronized static void setTomorrowArr(JSONArray tomorrowArr) {
        Parser.tomorrowArr = tomorrowArr;
    }


    public static String processRequest(ScheduleRequest request) {
        int targetGroup;

        String reqGroup = request.getGroup();
        String reqDate = request.getDate();

        if (getTodayArr().size() == 8) {
            getTodayArr().add("");
        }
        if (getTomorrowArr().size() == 8) {
            getTomorrowArr().add("");
        }


        switch (reqGroup) {
            case "11-801":
                targetGroup = 1;
                break;
            case "11-802":
                targetGroup = 2;
                break;
            case "11-803":
                targetGroup = 3;
                break;
            case "11-804":
                targetGroup = 4;
                break;
            case "11-805":
                targetGroup = 5;
                break;
            case "11-806":
                targetGroup = 6;
                break;
            case "11-807":
                targetGroup = 7;
                break;
            case "11-808":
                targetGroup = 8;
                break;
            case "11-809":
                targetGroup = 9;
                break;
            case "11-810":
                targetGroup = 10;
                break;
            case "11-811":
                targetGroup = 11;
                break;
            default:
                targetGroup = 11;
                break;
        }


        String timestampsUnformatted = getTodayArr().get(0).toString();
        String timestamps[] = timestampsUnformatted.substring(1, timestampsUnformatted.length() - 1).split(",");
        ArrayList<String> tsList = new ArrayList<String>();
        for (String s : timestamps)
            tsList.add(s.substring(1, s.length() - 1));

        String group1Unformatted, groupNUnformatted, group7Unformatted, group8Unformatted;

        if (reqDate.equals("Сегодня")) {
            if (getTodayArr().size() <= 1)
                return "Выходной";
            else {
                group1Unformatted = getTodayArr().get(1).toString();
                group7Unformatted = getTodayArr().get(7).toString();
                if (getTodayArr().size() == 9) {
                    group8Unformatted = group7Unformatted;
                    if (targetGroup > 7) {
                        groupNUnformatted = group7Unformatted;
                    }
                    else {
                        groupNUnformatted = getTodayArr().get(targetGroup).toString();
                    }
                }
                else {
                    group8Unformatted = getTodayArr().get(8).toString();
                    groupNUnformatted = getTodayArr().get(targetGroup).toString();
                }
            }
        } else {
            if (getTomorrowArr().size() <= 1)
                return "Выходной";
            else {
                group1Unformatted = getTomorrowArr().get(1).toString();
                group7Unformatted = getTomorrowArr().get(7).toString();
                if (getTomorrowArr().size() == 9) {
                    group8Unformatted = group7Unformatted;
                    if (targetGroup > 7) {
                        groupNUnformatted = group7Unformatted;
                    }
                    else {
                        groupNUnformatted = getTomorrowArr().get(targetGroup).toString();
                    }
                }
                else {
                    group8Unformatted = getTomorrowArr().get(8).toString();
                    groupNUnformatted = getTomorrowArr().get(targetGroup).toString();
                }
            }
        }

        // Извлекаемые из JSONArray строки приводятся к массиву, а потом к списку
        ArrayList<String> g1List = new ArrayList<String>();
        if (group1Unformatted.length() != 2) {
            String[] g1Arr = group1Unformatted.substring(2, group1Unformatted.length() - 2).split("\",\"");
            for (String s : g1Arr)
                g1List.add(s);
        }

        ArrayList<String> gNList = new ArrayList<>();

        if (groupNUnformatted.length() != 2) {
            String[] gNArr = groupNUnformatted.substring(2, groupNUnformatted.length() - 2).split("\",\"");
            for (String s : gNArr)
                gNList.add(s);
        }

        ArrayList<String> g7List = new ArrayList<String>();
        if (group7Unformatted.length() != 2) {
            String[] g7Arr = group7Unformatted.substring(2, group7Unformatted.length() - 2).split("\",\"");
            for (String s : g7Arr)
                g7List.add(s);
        }

        ArrayList<String> g8List = new ArrayList<>();
        if (group8Unformatted.length() != 2) {
            String[] g8Arr = group8Unformatted.substring(2, group8Unformatted.length() - 2).split("\",\"");
            for (String s : g8Arr)
                g8List.add(s);
        }


        boolean secondStream = targetGroup >= 7; // Второй поток?
        boolean cSharpStream = targetGroup > 7;  // Поток C#?
        return formSchedule(g1List, gNList, tsList, g7List, secondStream, cSharpStream, g8List);


    }

    public static void update() throws ParseException {
        Date date = new Date();
        String today = date.toString().substring(0, 3);
        String tomorrow = dOW.get((dOW.indexOf(today) + 1) % dOW.size());

        // getting today
        String URL = GAPI.START + daysOfWeek.get(today) + GAPI.DIMENSION + GAPI.KEY;
        String response = Requests.get(URL).send().readToText();
        Object obj = new JSONParser().parse(response);
        JSONObject jo = (JSONObject) obj;
        setTodayArr((JSONArray) jo.get("values"));

        //getting tomorrow
        URL = GAPI.START + daysOfWeek.get(tomorrow) + GAPI.DIMENSION + GAPI.KEY;
        response = Requests.get(URL).send().readToText();
        obj = new JSONParser().parse(response);
        jo = (JSONObject) obj;
        setTomorrowArr((JSONArray) jo.get("values"));
    }

    // Ниже ужасный код, мне за него стыдно
    private static String formSchedule(ArrayList<String> group1, ArrayList<String> groupN, ArrayList<String> ts,
                                       ArrayList<String> group7, boolean secStream, boolean cSharp,
                                       ArrayList<String> group8) {
        for (int i = 0; i < ts.size(); i++) {
            if (groupN.size() <= i) { // если в группе Н элементов меньше чем i
                if (cSharp) {
                    if (!(group7.size() <= i) && !group7.get(i).isEmpty()) {
                        if (group7.get(i).split(" ").length > 5 || group7.get(i).contains("ауд.108")) {
                            groupN.add(group7.get(i));
                        }
                    } else if (!(group8.size() <= i) && group8.get(i).split(" ").length > 7) { // если в первой группе элементов не меньше i
                        groupN.add(group8.get(i));
                    } else if (!(group1.size() <= i) && !group1.get(i).equals("")) {
                        if (group1.get(i).split(" ").length > 8 && (group1.get(i).contains("Физическая культура") ||
                                group1.get(i).contains("История") || group1.get(i).contains("русский язык"))) {
                            groupN.add(group1.get(i));
                        }
                        if (group1.get(i).contains("проектную деятельность")) {
                            groupN.add(group1.get(i));
                        }
                    } else {
                        groupN.add("");
                    }
                } else if (!secStream) {
                    if (!(group1.size() <= i)) { // если в первой группе элементов не меньше i
                        if (group1.get(i).split(" ").length > 7 || group1.get(i).contains("Физическая культура")) {
                            groupN.add(group1.get(i));
                        } else {
                            groupN.add("");
                        }
                    }
                } else {
                    if (!(group7.size() <= i) && !group7.get(i).equals("")) {
                        if (group7.get(i).split(" ").length > 8) {
                            groupN.add(group7.get(i));
                        }
                    } else if (!(group1.size() <= i) && !group1.get(i).equals("")) {
                        if (group1.get(i).split(" ").length > 8 || group1.get(i).contains("Физическая культура")) {
                            groupN.add(group1.get(i));
                        }
                    } else {
                        groupN.add("");
                    }
                }
                groupN.add("");
            } else if (groupN.get(i).equals("")) {
                if (cSharp) {
                    if (!(group7.size() <= i) && !group7.get(i).equals("")) {
                        if (group7.get(i).split(" ").length > 5) {
                            groupN.remove(i);
                            groupN.add(i, group7.get(i));
                        }
                    } else if (!(group8.size() <= i) && !group8.get(i).equals("")) {
                        if (group8.get(i).split(" ").length > 8) {
                            groupN.remove(i);
                            groupN.add(i, group8.get(i));
                        }
                    } else if (!(group1.size() <= i) && !group1.get(i).equals("")) {
                        if (group1.get(i).split(" ").length > 8 || group1.get(i).contains("Физическая культура")) {
                            groupN.remove(i);
                            groupN.add(i, group1.get(i));
                        }
                    }

                } else if (!secStream) {
                    if (!(group1.size() <= i)) {
                        if (group1.get(i).split(" ").length > 8 || group1.get(i).contains("Физическая культура")) {
                            groupN.remove(i);
                            groupN.add(i, group1.get(i));
                        }
                    }
                } else {
                    if (!(group7.size() <= i) && !group7.get(i).equals("")) {
                        if (group7.get(i).split(" ").length > 8) {
                            groupN.remove(i);
                            groupN.add(i, group7.get(i));
                        }
                    } else if (!(group1.size() <= i) && !group1.get(i).equals("")) {
                        if (!group1.get(i).contains("Алгебра") && !group1.get(i).contains("Математичесий") &&
                                !group1.get(i).contains("Дискретная")) {
                            if (group1.get(i).split(" ").length > 8 || group1.get(i).contains("Физическая культура")) {
                                groupN.remove(i);
                                groupN.add(i, group1.get(i));
                            }
                        } else {
                            groupN.remove(i);
                        }
                    }
                }
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < groupN.size(); i++) {
            if (!groupN.get(i).equals("") && !groupN.get(i).replaceAll(" ", "").equals("")) {
                stringBuilder.append(ts.get(i))
                        .append(": ")
                        .append(groupN.get(i).replaceAll("\\\\n", ""))
                        .append("\n")
                        .append("\n");
            }
        }
        String result = stringBuilder.toString();
        if (result.replaceAll(" ", "").equals("")) {
            return "Выходной";
        } else {
            return result;
        }
    }
}
